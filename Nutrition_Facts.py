#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

data = pd.read_csv("starbucks_drinkMenu_expanded.csv")

data.shape[0]

list(data.columns)

data.info()

data.head()

df=data[['Calories','Trans Fat (g) ','Saturated Fat (g)', ' Sodium (mg)', ' Total Carbohydrates (g) ','Cholesterol (mg)',' Dietary Fibre (g)',' Sugars (g)',' Protein (g) ',]]

df.head()

df.corr(method ='pearson')

df.plot(kind='scatter',x='Cholesterol (mg)',y='Calories',color='red')
df.plot(kind='scatter',x=' Sugars (g)',y='Calories',color='blue')
plt.show()

df_x=data[['Trans Fat (g) ','Saturated Fat (g)', ' Sodium (mg)', ' Total Carbohydrates (g) ','Cholesterol (mg)',' Dietary Fibre (g)',' Sugars (g)',' Protein (g) ',]]

df_x.head()

df_y=data[['Calories']]

df_y.head()

reg = linear_model.LinearRegression()

x_train,x_test,y_train,y_test = train_test_split(df_x,df_y,test_size = 0.3,random_state = 5)

reg.fit(x_train,y_train)

reg.score(x_train,y_train)
